package com.company;

import com.company.maket.Book;
import com.company.maket.Literature;
import com.company.maket.Magazine;
import com.company.maket.Yearbook;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Literature[] literatures = new Literature[6];

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите год издания: ");
        int year = Integer.parseInt(scan.nextLine());

        System.out.println("Введите категорию (Книга, журнал, ежегодник): ");
        String category = scan.nextLine();

        literatures[0] = new Book("Книга", "Преступление и наказание", 1800, "Достоевский", "Ранок");
        literatures[1] = new Book("Книга", "Война и мир", 1889, "Лев Толстой", "Правда");

        literatures[2] = new Magazine("Журнал", "Двойник", 1930, "Январь", "Детектив");
        literatures[3] = new Magazine("Журнал", "Про Volkswagen", 1980, "Июль", "Авто");

        literatures[4] = new Yearbook("Ежегодник", "Планы на год", 2005, "Работа", "Успешный бизнесмен");
        literatures[5] = new Yearbook("Ежегодник", "Планы на будущее", 2015, "Жизнедеятельность", "Успешная жизнь");


        for (Literature literature : literatures) {
            if (literature.isOlderThen(year)) {
                System.out.println("Поиск по году: " + literature);
            }
            if (literature.category(category)) {
                System.out.println("Поиск по категории: " + literature);
            }
        }
    }
}

