package com.company.maket;


public class Book extends Literature {

    private String author;
    private String publisher;

    public Book(String type, String name, int yearOfPublishing, String author, String publisher) {
        super(type, name, yearOfPublishing);
        this.author = author;
        this.publisher = publisher;
    }


    @Override
    public String toString() {
        return String.format("%s; Автор: %s; Издательство: %s", super.toString(), author, publisher);
    }
}
