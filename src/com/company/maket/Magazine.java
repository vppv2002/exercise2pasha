package com.company.maket;


public class Magazine extends Literature {
    private String monthOfPublishing;
    private String theme;

    public Magazine(String type, String name, int yearOfPublishing, String monthOfPublishing, String theme) {
        super(type, name, yearOfPublishing);
        this.monthOfPublishing = monthOfPublishing;
        this.theme = theme;
    }

    @Override
    public String toString() {
        return String.format("%s; Месяц издания: %s; Тематика: %s", super.toString(), monthOfPublishing, theme);
    }
}
