package com.company.maket;


public class Literature {
    private String name;
    private int yearOfPublishing;
    private String type;

    public Literature(String type, String name, int yearOfPublishing) {
        this.type = type;
        this.name = name;
        this.yearOfPublishing = yearOfPublishing;
    }

    @Override
    public String toString() {
        return String.format("Название: %s; Год издания: %d", name, yearOfPublishing);
    }

    public boolean isOlderThen(int year) {
        return yearOfPublishing == year;
    }

    public boolean category(String category) {
        return category.equalsIgnoreCase(type);
    }
}

