package com.company.maket;

public class Yearbook extends Literature {
    private String theme;
    private String publisher;

    public Yearbook(String type, String name, int yearOfPublishing, String theme, String publisher) {
        super(type, name, yearOfPublishing);
        this.theme = theme;
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return String.format("%s; Издательство: %s; Тематика: %s", super.toString(), publisher, theme);
    }
}
